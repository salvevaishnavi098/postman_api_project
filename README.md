##Title :- Git_Postman_Project
##Description:-
This project contains execution of number of test cases by using postman software with rest API .In this we implemented different method of rest API like POST,GET,PUT,PATCH and DELETE as well as it includated data driven menthod as well.
##Installation and Steps :-
For rest API execution:-
Installation of postman and do login with ID and Password .
Create collection with different test cases with creat data by POST,change of request by PUT and PATCH, Delte data by DELETE application and same for data driven testing .
After collection  environment is created .
Export both collection and environment in json file.
##Git bash Steps for repository
//Clone git repository into local system
Git clone
//Checkin the main branch
Git cd (project file name)
//Creat child branch
Git Checkout -b "name of child branch"
//add code file into branch
Git add*
//To check file added or not
Git status
//Need to commit file
Git commit -m "Name of the Message"
Git push origin
##Technologies Usage
postman (Rest API application for testing)
Newman noteJS

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
